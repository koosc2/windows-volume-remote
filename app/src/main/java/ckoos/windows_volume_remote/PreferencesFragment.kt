package ckoos.windows_volume_remote

import android.os.Bundle
import android.preference.PreferenceActivity
import android.preference.PreferenceFragment
import android.preference.PreferenceManager
import android.content.SharedPreferences
import android.support.v7.preference.ListPreference
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat
import android.util.Log
import net.mm2d.upnp.Adapter.discoveryListener
import net.mm2d.upnp.ControlPoint
import net.mm2d.upnp.ControlPointFactory
import net.mm2d.upnp.Protocol
import java.util.*


class SettingsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {

    internal lateinit var sharedPreferences: SharedPreferences

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        //add xml
        addPreferencesFromResource(R.xml.preferences)


//        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity())

    }

    override fun onResume() {
        super.onResume()

        Log.d("wvr-preferenes", "pref")
        //unregister the preferenceChange listener
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this)

        sharedPreferences = getPreferenceScreen().getSharedPreferences()

        val preference = findPreference("host_location")
        preference.summary = sharedPreferences.getString("host_location", preference.summary.toString())

        preference.setOnPreferenceChangeListener { new_preference, newValue ->

            preference.summary = newValue.toString()
            true
        }

        val cp = ControlPointFactory.create(
                protocol = Protocol.IP_V4_ONLY
        )
        cp.initialize()

//        val udaType = UDADeviceType("WindowsVolumeEndpoint")

//ControlPoint.DiscoveryListener

        cp.addDiscoveryListener(discoveryListener({ searchListener() }, { searchListener() }))

        cp.start()
        cp.search(null)



//        cp.search(udaType.toString() )
//
//        cp.deviceList




    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        val preference = findPreference(key)
    }

    override fun onPause() {
        super.onPause()
        //unregister the preference change listener
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this)
    }



    companion object {
        private val TAG = SettingsFragment::class.java.simpleName
    }

    private fun searchListener() {
        Log.d("wvr-preferences", "reached listener")
//        cp.deviceList.forEach{
//            Log.d("wvr-preferences", "device: ${it}")
//        }

    }
}