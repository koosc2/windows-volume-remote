package ckoos.windows_volume_remote

import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import ckoos.windows_volume_remote.sync.VolumeApplication
import android.widget.TextView
import ckoos.windows_volume_remote.sync.ApplicationSync
import android.util.Log
import android.widget.ImageView
import java.net.ConnectException
import java.net.InetSocketAddress
import java.net.Socket

class ApplicationAdapter(private val dataSource: ArrayList<VolumeApplication>) : RecyclerView.Adapter<ApplicationAdapter.ViewHolder>() {
    lateinit var appSync: ApplicationSync

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ApplicationAdapter.ViewHolder {

        // create a new view
        val inflater = LayoutInflater.from(parent.context)

        val listItem = inflater.inflate(R.layout.application_row, parent, false)




        return ViewHolder(listItem)
    }

    override fun getItemCount(): Int {
        return dataSource.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setName(this.dataSource.get(position).name)
        holder.setIcon(this.dataSource.get(position).icon)
        holder.seekBar.setProgress(this.dataSource.get(position).volume, true)
        holder.seekBar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                // Write code to perform some action when progress is changed.
            }
            override fun onStartTrackingTouch(seekBar: SeekBar) {
                // Write code to perform some action when touch is started.
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                // Write code to perform some action when touch is stopped.
                Log.d("ApplicationAdapter", "Setting volume to $seekBar.progress")
                appSync.setVolume(dataSource.get(position).pid, seekBar.progress)
            }
        })


    }

    fun addItem(item: VolumeApplication) {
        this.dataSource.add(item)
        this.notifyDataSetChanged()
    }

    fun clear() {
        this.dataSource.clear()
    }

    fun addItems(items: ArrayList<VolumeApplication>) {
        for (item in items) {
            this.dataSource.add(item)
        }
        this.notifyDataSetChanged()
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var textView = view.findViewById<TextView>(R.id.applicationName)
        var seekBar = view.findViewById<SeekBar>(R.id.applicationVolume)
        var appIcon = view.findViewById<ImageView>(R.id.appIcon)

        fun setName(string: String) {
            this.textView.setText(string)
        }

        fun setIcon (icon: String) {
            if (icon != "null") {
                val decodedString = Base64.decode(icon, Base64.DEFAULT)
                val decodedBytes = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                this.appIcon.setImageBitmap(decodedBytes)


                Log.d("wvr-ApplicationAdapter", "Setting icon: ${icon}")
            }
            else {
                Log.d("wvr-ApplicationAdapter", "icon null: ${icon}")
            }
        }
    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }



}