package ckoos.windows_volume_remote.sync
import android.util.Log
import ckoos.windows_volume_remote.ApplicationAdapter
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.result.Result
import org.json.JSONArray
import org.json.JSONObject
import java.net.ConnectException
import java.net.InetSocketAddress
import java.net.Socket

class ApplicationSync (hostLocation: String, adapter: ApplicationAdapter) {

    val hostConnection: String
    val hostPort: Int = 12345
    lateinit var adapter: ApplicationAdapter

    var applications: JSONArray = JSONArray()
    init {
        this.hostConnection = "http://$hostLocation:${hostPort}"
        this.adapter = adapter

        if (true) {
            getApplications()
        }
        else {
            Log.e("wvr-ApplicationSync", "Port $hostPort not open on $hostLocation")
        }

    }


    fun getApplicationList() : ArrayList<VolumeApplication>{
        var applicationList = ArrayList<VolumeApplication>()
        Log.d("applicationsync", "apps")
        Log.d("applicationsync", applications.toString())
        for (i in 0..(applications.length() - 1)) {
//            var app = VolumeApplication()
            var app = applications.getJSONObject(i)
            var vApp = VolumeApplication(app.getString("name"), app.getInt("pid"), app.getInt("volume"), app.getString("icon"))
            vApp.volume = app.getInt("volume")
            applicationList.add(vApp)
        }
        Log.d("applicationsync", "list")
        Log.d("applicationsync", applicationList.toString())
        Log.d("applicationsync", applications.toString())
        return applicationList
    }

    fun setVolume(application: Int, desiredVolume: Int) {

        val setVolumeBody = JSONObject()
        setVolumeBody.put("application", application)
        setVolumeBody.put("volume", desiredVolume)

        if (hostConnection == "") {
            Log.e("applicationsync", "Host has to be registered before calls can be made")
        }
        else {
            Fuel.post("$hostConnection/setVolume").body(setVolumeBody.toString())
            Fuel.post("$hostConnection/setVolume").body(setVolumeBody.toString()).responseString {request, response, result ->
                Log.d("applicationsync", result.get())
            }
            Log.d("applicationsync", "Set volume!")
        }
    }

    private fun getApplications() {
        val getApplicationEndpoint = "/getApplications"

        Fuel.get(hostConnection + getApplicationEndpoint).responseString {request, response, result ->
            when (result) {
                is Result.Failure -> {
                    Log.e("wvr-ApplicationSync", "Error getting applications: ${result.getException()}")
                    // Not sure why this was here. Shouldn't have any response?
//                    adapter.addItems(ArrayList<VolumeApplication>())
//                    adapter.notifyDataSetChanged()
                    
                }
                is Result.Success -> {
                    Log.d("applicationsync", result.get())
                    val applicationsJson: JSONArray = JSONArray(result.get())
                    this.applications = applicationsJson
                    adapter.clear()
                    adapter.addItems(this.getApplicationList())
                    adapter.notifyDataSetChanged()

//            set(applications) { return applicationsJson }
                    Log.d("applicationsync", "applications")
                    Log.d("applicationsync", this.applications.toString())
//            Log.d("applicationsync", applicationsJson.getJSONObject(0).getString("name"))
                    //do something with response
//            result.fold({ d ->
//                Log.d("applicationsync", d.toString())
//                //do something with data
//            }, { err ->
//                //do something with error
//                null
//            })
//            })
                    Log.d("applicationsync", "Got applications")
                }
            }

        }

    }


}
