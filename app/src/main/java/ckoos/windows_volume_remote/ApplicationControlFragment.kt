package ckoos.windows_volume_remote
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ckoos.windows_volume_remote.sync.ApplicationSync
import ckoos.windows_volume_remote.sync.VolumeApplication
import kotlinx.android.synthetic.main.application_control_fragment.*
import android.content.SharedPreferences
import android.content.Context.MODE_PRIVATE
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.preference.PreferenceManager
import android.util.Log
import android.widget.Toast
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock


class ApplicationControlFragment : Fragment() {

    lateinit var appConnection: ApplicationSync
    lateinit var viewAdapter: ApplicationAdapter
    var updateLock: Lock = ReentrantLock()

    //2
    companion object {

        fun newInstance(): ApplicationControlFragment {
            return ApplicationControlFragment()
        }
    }

    //3
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        var view: View = inflater.inflate(R.layout.application_control_fragment, container, false)


        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        lateinit var viewManager: RecyclerView.LayoutManager
        // TODO Convert this to lambda
        swipeContainer.setOnRefreshListener(object: SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                Log.d("wvr-ApplicationControlFragment", "Refreshing...")
                connectToHost()
                swipeContainer.isRefreshing = false
            }
        })

        viewAdapter = ApplicationAdapter(ArrayList<VolumeApplication>())


        connectToHost()


//        var applicationData: ArrayList<VolumeApplication> = appConnection.getApplicationList()


//        var testVol: VolumeApplication = VolumeApplication()



        viewManager = LinearLayoutManager(this.context)
//        viewAdapter = ApplicationAdapter(applicationData)
        applicationRecycler.layoutManager = viewManager

        applicationRecycler.apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter

        }

    }

    fun connectToHost() {

        viewAdapter.clear()
        val prefs = PreferenceManager.getDefaultSharedPreferences(this.context)
        val hostLocation = prefs.getString("host_location", "")
        if (hostLocation == "") {
            Log.d("wvr-applicationControl", "Host location not yet set")
            Toast.makeText(this.context, "Please configure host in settings", Toast.LENGTH_LONG).show()
        }
        else {
            appConnection = ApplicationSync(hostLocation, viewAdapter)
            viewAdapter.appSync = appConnection
        }

    }
}